package main

import "testing"

func TestCreateLinkedList(t *testing.T) {
	list := CreateLinkedList(1)
	if list.Head.data != 1 && list.length != 1 {
		t.Errorf("Expected to return %d as data and length %d but returned %d and %d", 1, 1, list.Head.data, list.length)
	}
}

func TestAddNode(t *testing.T) {
	list := CreateLinkedList(1)
	list.Add(2)
	list.Add(3)
	list.Add(4)
	if list.length != 4 {
		t.Errorf("Expected to return %d as length but returned %d", 4, list.length)
	}
}

func TestFindNode(t *testing.T) {
	list := CreateLinkedList(1)
	list.Add(2)
	list.Add(3)
	list.Add(4)
	if list.Find(3) != 2 {
		t.Errorf("Expected to return %d as index but returned %d", 2, list.Find(3))
	}
	if list.Find(5) != -1 {
		t.Errorf("Expected to return %d as index but returned %d", -1, list.Find(5))
	}
	if list.Find(1) != 0 {
		t.Errorf("Expected to return %d as index but returned %d", 0, list.Find(1))
	}
}

func TestDelete(t *testing.T) {
	list := CreateLinkedList(1)
	list.Add(2)
	list.Add(3)
	list.Add(4)
	list.Delete(3)
	if list.length != 3 {
		t.Errorf("Expected to return %d as length but returned %d", 3, list.length)
	}
}
