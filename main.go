package main

import "fmt"

type (
	// Box data type
	Box interface{}

	// Node
	Node struct {
		data Box
		next *Node
	}

	// LinkedList
	LinkedList struct {
		Head   *Node
		length int
	}
)

//CreateLinkedList create linked list
func CreateLinkedList(data Box) *LinkedList {
	head := &Node{data: data, next: nil}
	linkedList := LinkedList{Head: head, length: 1}
	return &linkedList
}

//Add to linked list
func (ll *LinkedList) Add(data Box) {
	if ll.Head != nil {
		tmp := ll.Head
		for tmp.next != nil {
			tmp = tmp.next
		}
		tmp.next = &Node{data: data, next: nil}
		ll.length++
	} else {
		ll.Head = &Node{data: data, next: nil}
		ll.length = 1
	}
}

//Find to linked list
func (ll *LinkedList) Find(data Box) int {
	index := -1
	if ll.Head != nil {
		tmp := ll.Head
		i := 0
		for tmp.next != nil {
			if tmp.data == data {
				return i
			}
			tmp = tmp.next
			i++
		}
	}
	return index
}

//Delete to linked list
func (ll *LinkedList) Delete(data Box) {
	if ll.Head.data == data {
		if ll.Head.next != nil {
			ll.Head = ll.Head.next
			ll.length--
		}
	}
	if ll.length > 1 {
		tmp := ll.Head.next
		oldTmp := ll.Head
		for tmp != nil {
			if tmp.data == data {
				oldTmp.next = tmp.next
				ll.length--
				return
			}
			oldTmp = tmp
			tmp = tmp.next
		}
	}
}

//Find to linked list
func (ll *LinkedList) Print() {
	if ll.Head != nil {
		tmp := ll.Head
		i := 0
		for tmp != nil {
			fmt.Println("Index ", i, " data ", tmp.data)
			tmp = tmp.next
			i++
		}
	}
}

func main() {
	ll := CreateLinkedList(1)
	ll.Add(2)
	ll.Add(3)
	ll.Add(4)
	ll.Add(5)
	ll.Add(6)
	ll.Add(7)
	ll.Delete(4)
	ll.Print()
	fmt.Println("===========================")
	ll.Delete(1)
	ll.Print()
	fmt.Println("===========================")
	ll.Delete(7)
	ll.Print()
	fmt.Println("===========================")
}
